﻿Public Class Window1
    Public viewmodel As Window1ViewModel

    Public Sub New(actual As MaterialesWPF.Materiales)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub Window1_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        viewmodel = New Window1ViewModel(modelo, materialActual)
        grid2.DataContext = viewmodel
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        viewmodel.Devolver()
        'Me.Close()
    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs)
        viewmodel.Retirar()

    End Sub
End Class
