﻿Imports System.ComponentModel

Public Class Window1ViewModel
    Implements INotifyPropertyChanged
    Private modelo As MaterialesWPF.DAM_Andermaestre_DEVEntities
    Private materialactual As MaterialesWPF.Materiales
    Public Sub New(m As MaterialesWPF.DAM_Andermaestre_DEVEntities, act As MaterialesWPF.Materiales)
        modelo = m
        materialactual = act

        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Prestado"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Marca"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Descripcion"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Estado"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Disponible"))
    End Sub

    Public ReadOnly Property Descripcion
        Get
            Return materialactual.Descripcion
        End Get
    End Property

    Public ReadOnly Property Marca
        Get
            Return materialactual.Marca
        End Get
    End Property
    Public ReadOnly Property Estado
        Get
            Return materialactual.Estados.Descripcion
        End Get
    End Property

    Public ReadOnly Property Prestado
        Get
            If materialactual.Estados.Descripcion.CompareTo("PRESTADO") Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property Disponible
        Get
            If materialactual.Estados.Descripcion.CompareTo("DISPONIBLE") Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property



    Public Sub Devolver()


        Dim nuevoMovimiento As New MaterialesWPF.Movimientos


        If modelo.Movimientos.Count <> 0 Then
            nuevoMovimiento.IdMovimiento = modelo.Movimientos.Max(Function(x) x.IdMovimiento) + 1
        Else
            nuevoMovimiento.IdMovimiento = 1
        End If

        nuevoMovimiento.IdMaterial = materialactual.IdMaterial
        nuevoMovimiento.IdAccion = 2
        materialactual.IdEstado = 2
        nuevoMovimiento.Fecha = DateTime.Now

        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Prestado"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Disponible"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Estado"))

        modelo.Movimientos.Add(nuevoMovimiento)
        modelo.SaveChanges()



    End Sub

    Public Sub Retirar()


        Dim nuevoMovimiento As New MaterialesWPF.Movimientos


        If modelo.Movimientos.Count <> 0 Then
            nuevoMovimiento.IdMovimiento = modelo.Movimientos.Max(Function(x) x.IdMovimiento) + 1
        Else
            nuevoMovimiento.IdMovimiento = 1
        End If

        nuevoMovimiento.IdMaterial = materialactual.IdMaterial
        nuevoMovimiento.IdAccion = 1
        materialactual.IdEstado = 1
        nuevoMovimiento.Fecha = DateTime.Now

        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Prestado"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Disponible"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Estado"))

        modelo.Movimientos.Add(nuevoMovimiento)
        modelo.SaveChanges()



    End Sub


    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
End Class
