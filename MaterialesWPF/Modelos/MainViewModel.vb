﻿Imports MaterialesWPF.MaterialesWPF
Imports System.ComponentModel

Public Class MainViewModel
    Implements INotifyPropertyChanged

    Private modelo As MaterialesWPF.DAM_Andermaestre_DEVEntities
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub New(modelo As DAM_Andermaestre_DEVEntities)
        Me.modelo = modelo

    End Sub

    Public ReadOnly Property listaMateriales
        Get
            Return modelo.Materiales.ToArray()
        End Get
    End Property

End Class
